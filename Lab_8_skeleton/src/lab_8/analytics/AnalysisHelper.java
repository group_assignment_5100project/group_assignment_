/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lab_8.analytics;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import lab_8.entities.Comment;
import lab_8.entities.Post;
import lab_8.entities.User;

/**
 *
 * @author harshalneelkamal
 */
public class AnalysisHelper {
    
    public void findAverage(){
        Map<Integer,Comment> comment = DataStore.getInstance().getComments();
        int length = comment.size();
        int likes = 0;
        for(Comment c : comment.values()){
                likes += c.getLikes();
            }
        
         System.out.println("Average likes for comments:"+"\n"+likes/length+"\n");
    }
    public void Top5InactiveBsonCom(){
        Map<Integer,User> users = DataStore.getInstance().getUsers();
        List<User> list = new ArrayList<>();
        for(User u:users.values()){
            list.add(u);
        }
        Collections.sort(list,new Comparator<User>(){
        @Override
        public int compare(User o1, User o2){
           return Integer.compare(o2.getComments().size(), o1.getComments().size());
       }
    });
        System.out.println("Top5 Inactive User based on comments:");
        System.out.println(list.get(0));
        System.out.println(list.get(1));
        System.out.println(list.get(2));
        System.out.println(list.get(3));
        System.out.println(list.get(4));
        System.out.println();
    }
    public void userWithMostLikes(){
        Map<Integer,Integer> userLikecount = new HashMap<Integer,Integer>();
        Map<Integer,User> users = DataStore.getInstance().getUsers();
        for(User user: users.values()){
            for(Comment c : user.getComments()){
                int likes =0;
                if(userLikecount.containsKey(user.getId()))
                    likes = userLikecount.get(user.getId());
                likes += c.getLikes();
                userLikecount.put(user.getId(), likes);
            }
        }
        int max = 0;
        int maxId = 0;
        for (int id : userLikecount.keySet()){
            if(userLikecount.get(id) > max){
            max  = userLikecount.get(id);
            maxId = id;
        }
            
        }
        System.out.println("User with most likes :"+max+"\n"+users.get(maxId)+"\n");
        
    }
    
    public void getFiveMostLikedComment(){
       Map<Integer, Comment> comments = DataStore.getInstance().getComments();
       List<Comment> commentList = new ArrayList<>(comments.values());
       Collections.sort(commentList, new Comparator<Comment>() {
           @Override
           public int compare(Comment o1, Comment o2) {
               //so as to get decending list
               return o2.getLikes() - o1.getLikes();
           }
       });
       System.out.println("FiveMostLikedComment"+"\n"+commentList.get(0)+"\n"+
               commentList.get(1)+"\n"+commentList.get(2)+"\n"+commentList.get(3)+
               "\n"+commentList.get(4)+"\n");
        
    }

    public void getMostLikedComment() {
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
       List<Comment> commentList = new ArrayList<>(comments.values());
       Collections.sort(commentList, new Comparator<Comment>() {
           @Override
           public int compare(Comment o1, Comment o2) {
               //so as to get decending list
               return o2.getLikes() - o1.getLikes();
           }
       });
       System.out.println("MostLikedComment"+"\n"+commentList.get(0)+"\n");
        
    }

    public void Postswithmostcomments() {
       Map<Integer,Post> posts = DataStore.getInstance().getPosts();
       List<Post> postList = new ArrayList<>();
       int i = 0;
       for (Post p : posts.values()) postList.add(p);
       Collections.sort(postList,new Comparator<Post>(){
        @Override
        public int compare(Post o1, Post o2){
           return Integer.compare(o2.getUserId(), o1.getUserId());
       }
       });
       System.out.println("Post/Posts with most comments: ------------");
       System.out.println(postList.get(0));
       while(i<=postList.size()){
           if(postList.get(i).getComments().size()== postList.get(i+1).getComments().size()){
               System.out.println(postList.get(i+1));
               i++;
           }
           else
               break;
    }
     System.out.println();
    }

    public void Top5inactiveusersbasedonposts() {
         Map<Integer,Post> posts = DataStore.getInstance().getPosts();
         Map<Integer,User> users = DataStore.getInstance().getUsers();
         Map<Integer,Integer> user = new HashMap<Integer,Integer>();
         List<Post> postList = new ArrayList<>();
         int number =0;
         for (Post p:posts.values()){
             postList.add(p);
         }
          Collections.sort(postList,new Comparator<Post>(){
        @Override
        public int compare(Post o1, Post o2){
           return Integer.compare(o2.getUserId(), o1.getUserId());
       }
       });
          int maxId = postList.get(0).getUserId();
          for(int i =0;i<maxId;i++){
              for(Post po:posts.values()){
                  if(po.getUserId() == i ){
                      number = number+1;
                  }
              }
              user.put(i, number);
              number = 0;
          }
          System.out.println(user);
          Comparator<Map.Entry<Integer,Integer>> valueComparator = new Comparator<Map.Entry<Integer,Integer>>(){
             @Override
             public int compare(Map.Entry<Integer,Integer>o1,Map.Entry<Integer,Integer> o2){
                 return o1.getValue()- o2.getValue();
             }
          };
          List<Map.Entry<Integer,Integer>> list= new ArrayList<Map.Entry<Integer,Integer>>(user.entrySet());
          Collections.sort(list,valueComparator);
          System.out.println("Top 5 inactive users based on posts");
          System.out.println(users.get(list.get(0).getKey()));
          System.out.println(users.get(list.get(1).getKey()));
          System.out.println(users.get(list.get(2).getKey()));
          System.out.println(users.get(list.get(3).getKey()));
          System.out.println(users.get(list.get(4).getKey()));
          System.out.println();
}


    public void fiveinactoverall(){
       Map<Integer, Comment> comments = DataStore.getInstance().getComments();
       Map<Integer, Post> posts = DataStore.getInstance().getPosts();
       Map<Integer,Integer> res = new HashMap<Integer,Integer>();
       List<Comment> commentList = new ArrayList<>(comments.values());
       List<Post> postList = new ArrayList<>(posts.values());
       for(Comment c: commentList){
           if(res.containsKey(c.getUserId()))
               res.put(c.getUserId(), res.get(c.getUserId())+2);
           else
               res.put(c.getUserId(), 2);
       }
       for(Post p: postList){
           if(res.containsKey(p.getUserId()))
               res.put(p.getUserId(), res.get(p.getUserId())+3);
           else
               res.put(p.getUserId(), 3);
       }
       for(Comment c: commentList){
           if(res.containsKey(c.getUserId()))
               res.put(c.getUserId(), res.get(c.getUserId())+c.getLikes());
           else
               res.put(c.getUserId(), 1);
       }
       List<Map.Entry<Integer,Integer>> list = new ArrayList<>(res.entrySet());
       
       Collections.sort(list, new Comparator<Map.Entry<Integer,Integer>>() {
           @Override
           public int compare(Entry o1, Entry o2) {
               //so as to get decending list
               return (int)o1.getValue() - (int)o2.getValue();
           }
       });
        System.out.println("Five Most inactive User:(post 3 points, comment 2 points, 1 like 1 point)"+"\nId:"+list.get(0).getKey()+" Score:"+list.get(0).getValue()
                +"\nId:"+list.get(1).getKey()+" Score:"+list.get(1).getValue()
                +"\nId:"+list.get(2).getKey()+" Score:"+list.get(2).getValue()
                +"\nId:"+list.get(3).getKey()+" Score:"+list.get(3).getValue()
                +"\nId:"+list.get(4).getKey()+" Score:"+list.get(4).getValue()+"\n");
    }

    
    public void fiveproactoverall(){
        Map<Integer, Comment> comments = DataStore.getInstance().getComments();
       Map<Integer, Post> posts = DataStore.getInstance().getPosts();
       Map<Integer,Integer> res = new HashMap<Integer,Integer>();
       List<Comment> commentList = new ArrayList<>(comments.values());
       List<Post> postList = new ArrayList<>(posts.values());
       for(Comment c: commentList){
           if(res.containsKey(c.getUserId()))
               res.put(c.getUserId(), res.get(c.getUserId())+2);
           else
               res.put(c.getUserId(), 2);
       }
       for(Post p: postList){
           if(res.containsKey(p.getUserId()))
               res.put(p.getUserId(), res.get(p.getUserId())+3);
           else
               res.put(p.getUserId(), 3);
       }
       for(Comment c: commentList){
           if(res.containsKey(c.getUserId()))
               res.put(c.getUserId(), res.get(c.getUserId())+c.getLikes());
           else
               res.put(c.getUserId(), 1);
       }
       List<Map.Entry<Integer,Integer>> list = new ArrayList<>(res.entrySet());
       
       Collections.sort(list, new Comparator<Map.Entry<Integer,Integer>>() {
           @Override
           public int compare(Entry o1, Entry o2) {
               //so as to get decending list
               return (int)o2.getValue() - (int)o1.getValue();
           }
       });
        System.out.println("Five Most proactive User:(post 3 points, comment 2 points, 1 like 1 point)"+"\nId:"+list.get(0).getKey()+" Score:"+list.get(0).getValue()
                +"\nId:"+list.get(1).getKey()+" Score:"+list.get(1).getValue()
                +"\nId:"+list.get(2).getKey()+" Score:"+list.get(2).getValue()
                +"\nId:"+list.get(3).getKey()+" Score:"+list.get(3).getValue()
                +"\nId:"+list.get(4).getKey()+" Score:"+list.get(4).getValue()+"\n");
    }
    
}
