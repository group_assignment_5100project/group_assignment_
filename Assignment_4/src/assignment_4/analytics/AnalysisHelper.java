/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4.analytics;

import assignment_4.entities.Order;
import assignment_4.entities.Product;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author haowenchang
 */
public class AnalysisHelper {
    public void topthreeproduct(){
        Map<Integer,Integer> t3p = new HashMap<Integer,Integer>();
        Map<Integer,Order> order = Datastore.getInstance().getOrders();
        for(Order o: order.values()){
            if(t3p.containsKey(o.getItem().getProductId()))
                t3p.put(o.getItem().getProductId(), o.getItem().getQuantity()+t3p.get(o.getItem().getProductId()));
            else
                t3p.put(o.getItem().getProductId(), o.getItem().getQuantity());
        }
        Comparator<Map.Entry<Integer,Integer>> valueComparator = new Comparator<Map.Entry<Integer,Integer>>(){
             @Override
             public int compare(Map.Entry<Integer,Integer>o1,Map.Entry<Integer,Integer> o2){
                 return o2.getValue()- o1.getValue();
             }
          };
        List<Map.Entry<Integer,Integer>> list= new ArrayList<Map.Entry<Integer,Integer>>(t3p.entrySet());
        Collections.sort(list,valueComparator);
        System.out.println("Top 3 product");
        System.out.println("Product id: "+list.get(0).getKey()+" Number: "+t3p.get(list.get(0).getKey()));
        System.out.println("Product id: "+list.get(1).getKey()+" Number: "+t3p.get(list.get(1).getKey()));
        System.out.println("Product id: "+list.get(2).getKey()+" Number: "+t3p.get(list.get(2).getKey()));

    }
    
    public void topthreecust(){
        Map<Integer, Integer> t3c = new HashMap<Integer,Integer>();
        Map<Integer,Order> order = Datastore.getInstance().getOrders();
        
        for(Order o: order.values()){
            if(t3c.containsKey(o.getCustomerId())){                
                t3c.put(o.getCustomerId(), t3c.get(o.getCustomerId())+
                        (o.getItem().getQuantity()*o.getItem().getSalesPrice()));                              
            }
            else
                t3c.put(o.getCustomerId(), o.getItem().getQuantity()*o.getItem().getSalesPrice());
        }
        Comparator<Map.Entry<Integer,Integer>> valueComparator = new Comparator<Map.Entry<Integer,Integer>>(){
             @Override
             public int compare(Map.Entry<Integer,Integer>o1,Map.Entry<Integer,Integer> o2){
                 return o2.getValue()- o1.getValue();
             }
          };
        List<Map.Entry<Integer,Integer>> list= new ArrayList<Map.Entry<Integer,Integer>>(t3c.entrySet());
        Collections.sort(list,valueComparator);
        System.out.println("Top 3 customer");
        System.out.println("Customer id: "+list.get(0).getKey()+" Consumption: "+t3c.get(list.get(0).getKey()));
        System.out.println("Customer id: "+list.get(1).getKey()+" Consumption: "+t3c.get(list.get(1).getKey()));
        System.out.println("Customer id: "+list.get(2).getKey()+" Consumption: "+t3c.get(list.get(2).getKey()));

    }
    
    
    public void topthreesale(){
            Map<Integer, Integer> t3s = new HashMap<Integer,Integer>();
        Map<Integer,Order> order = Datastore.getInstance().getOrders();
        Map<Integer,Product> product = Datastore.getInstance().getProducts();
        
        for(Order o: order.values()){
            int pid = o.getItem().getProductId();
            int sid = o.getItem().getSid();
            int price = o.getItem().getSalesPrice();
            int quantity = o.getItem().getQuantity();
            int target = product.get(pid).getTarget();
            if(t3s.containsKey(sid)){
                t3s.put(sid, t3s.get(sid)+(price-target)*quantity);
            }
            else
                t3s.put(sid, (price-target)*quantity);            
        }
        Comparator<Map.Entry<Integer,Integer>> valueComparator = new Comparator<Map.Entry<Integer,Integer>>(){
             @Override
             public int compare(Map.Entry<Integer,Integer>o1,Map.Entry<Integer,Integer> o2){
                 return o2.getValue()- o1.getValue();
             }
          };
        List<Map.Entry<Integer,Integer>> list= new ArrayList<Map.Entry<Integer,Integer>>(t3s.entrySet());
        Collections.sort(list,valueComparator);
        System.out.println("Top 3 salesman");
        System.out.println("Salesman id: "+list.get(0).getKey()+" Sales: "+t3s.get(list.get(0).getKey()));
        System.out.println("Salesman id: "+list.get(1).getKey()+" Sales: "+t3s.get(list.get(1).getKey()));
        System.out.println("Salesman id: "+list.get(2).getKey()+" Sales: "+t3s.get(list.get(2).getKey()));
    }
    
    public void revenue(){
        int revenue = 0;
        Map<Integer,Order> order = Datastore.getInstance().getOrders();
        Map<Integer,Product> product = Datastore.getInstance().getProducts();
        for(Order o: order.values()){
            int pid = o.getItem().getProductId();
            int quantity = o.getItem().getQuantity();
            int price = o.getItem().getSalesPrice();
            int min = product.get(pid).getMin();
            //System.out.println((price-min)+" "+quantity);
            revenue += quantity*(price-min);
        }
        System.out.print("Revenue\n"+revenue+"\n");
    
    }
}
