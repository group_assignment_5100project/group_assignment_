/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4.analytics;

import assignment_4.entities.Customer;
import assignment_4.entities.Item;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import assignment_4.entities.SalesPerson;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author haowenchang
 */
public class Datastore {
    public static Datastore datastore = new Datastore();
    
    private Map<Integer, Order> orders;
    //private Map<Integer, Order> orders;
    private Map<Integer, Product> products;
    private Map<Integer, SalesPerson> salesmen;
    private Map<Integer, Customer> customer;
    
    private Datastore(){
        orders = new HashMap<>();
        products = new HashMap<>();
        salesmen = new HashMap<>();
        customer = new HashMap<>();
    }
    
    public static Datastore getInstance(){
        if (datastore == null){
            datastore = new Datastore();
        }
        return datastore;
    }

    public static Datastore getDatastore() {
        return datastore;
    }

    public static void setDatastore(Datastore datastore) {
        Datastore.datastore = datastore;
    }

    public Map<Integer, Order> getOrders() {
        return orders;
    }

    public void setOrders(Map<Integer, Order> orders) {
        this.orders = orders;
    }

    public Map<Integer, Product> getProducts() {
        return products;
    }

    public void setProducts(Map<Integer, Product> products) {
        this.products = products;
    }

    public Map<Integer, SalesPerson> getSalesmen() {
        return salesmen;
    }

    public void setSalesmen(Map<Integer, SalesPerson> salesmen) {
        this.salesmen = salesmen;
    }

    public Map<Integer, Customer> getCustomer() {
        return customer;
    }

    public void setCustomer(Map<Integer, Customer> customer) {
        this.customer = customer;
    }
}
