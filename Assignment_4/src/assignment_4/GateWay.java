/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4;

import assignment_4.analytics.AnalysisHelper;
import assignment_4.analytics.Datastore;
import assignment_4.entities.Item;
import assignment_4.entities.Order;
import assignment_4.entities.Product;
import assignment_4.entities.SalesPerson;
import java.io.IOException;

/**
 *
 * @author harshalneelkamal
 */
public class GateWay {
    DataReader orderReader;
    DataReader productReader;
    AnalysisHelper helper;
    
    public GateWay()throws IOException{
        DataGenerator generator = DataGenerator.getInstance();
        orderReader = new DataReader(generator.getOrderFilePath());
        productReader = new DataReader(generator.getProductCataloguePath());
        helper = new AnalysisHelper();
    }
    
    public static void main(String args[]) throws IOException{
        
        //DataGenerator generator = DataGenerator.getInstance();
        
        //Below is the sample for how you can use reader. you might wanna 
        //delete it once you understood.
        GateWay inst = new GateWay();
        inst.readData();
       /* 
        DataReader orderReader = new DataReader(generator.getOrderFilePath());
        String[] orderRow;
        printRow(orderReader.getFileHeader());
        while((orderRow = orderReader.getNextRow()) != null){
            printRow(orderRow);
            generateorder(orderRow);
        }
        System.out.println("_____________________________________________________________");
        DataReader productReader = new DataReader(generator.getProductCataloguePath());
        String[] prodRow;
        printRow(productReader.getFileHeader());
        while((prodRow = productReader.getNextRow()) != null){
            printRow(prodRow);
        }
               */
    }
    
    private void readData() throws IOException{
        String[] row;
        while((row = orderReader.getNextRow()) != null ){
            generateorder(row);
            //generatesales(row);
            //generatecustomer(row);
            
        }
        while((row = productReader.getNextRow()) != null ){
            generateproduct(row);
            
        }
        
        runAnalysis();
    }
    private void generateorder(String[] row){
        int oid = Integer.parseInt(row[0]);
        int iid = Integer.parseInt(row[1]);
        int cid = Integer.parseInt(row[5]);
        Item item = new Item(Integer.parseInt(row[2]),Integer.parseInt(row[4]),Integer.parseInt(row[6]),Integer.parseInt(row[3]));
        Order order = new Order(oid,iid,cid,item);
        Datastore.getInstance().getOrders().put(oid,order);
    }
    private void generatesales(String[] row){
        int sid = Integer.parseInt(row[4]);
        int pid = Integer.parseInt(row[2]);
        int sq = Integer.parseInt(row[3]);
        int sp = Integer.parseInt(row[6]);
        SalesPerson s = new SalesPerson(sid,pid,sq,sp);
        Datastore.getInstance().getSalesmen().put(sid, s);
    }
    
    private void generatecustomer(String[] row){
        int cuid = Integer.parseInt(row[5]);
    }
    
    private void generateproduct(String[] row){
        int pid = Integer.parseInt(row[0]);
        int min = Integer.parseInt(row[1]);
        int max = Integer.parseInt(row[2]);
        int target = Integer.parseInt(row[3]);
        Product pro = new Product(pid,min,max,target);
        Datastore.getInstance().getProducts().put(pid, pro);
    }
    
    public static void printRow(String[] row){
        for (String row1 : row) {
            System.out.print(row1 + ", ");
        }
        System.out.println("");
    }
    private void runAnalysis(){
        System.out.print("\n");
        helper.topthreeproduct();
        System.out.print("\n");
        helper.topthreecust();
        System.out.print("\n");
        helper.topthreesale();
        System.out.print("\n");
        helper.revenue();
    }
    
}
